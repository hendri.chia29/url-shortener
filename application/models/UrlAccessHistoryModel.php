<?php

if(!defined("BASEPATH")) exit ("No direct script access allowed");

class UrlAccessHistoryModel extends CI_Model
{	private $table = "";

	public function __construct() {
    	parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
		$this->table = "url_access_history";
		$this->PK = "historyId";
		$this->FK = "urlid";
    }
	 
	function getStatistic($datain)
	{
		$data = 0;
		$sql = "select count(urlid) as cnt  from ".$this->table." mk  ";

		if($datain["urlid"]!="")
			$sql .=" where " .$this->FK. " =   '".trim($datain["urlid"]). "'";

		$sql = $sql. " group by urlid";
		$q = $this->db->query($sql);

		if($q->num_rows() > 0)
		{
			$row = $q->row_array();
			$data = $row["cnt"];
		}
		$q->free_result();
		return $data;
	}
	
	function addData($data)
	{
		if(!is_array($data)) return false;
	 	$this->db->trans_start();
		$this->db->insert($this->table, $data);
        $newID = $this->db->insert_id();
		$this->db->trans_complete();
		return $this->db->trans_status();
	}
}
?>