<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once "util_web.php" ;
class Core extends CI_Controller {

    private $errMsg="";
    private $succMsg="";
     
	public function __construct()
	{
   			parent::__construct();
            $this->load->helper('url');
            $this->load->library('session');
			$this->load->model('MsUrlModel',"msurl");
			$this->load->model('UrlAccessHistoryModel',"accesshistory");
            $this->load->helper("security");
            $this->load->database();
	}
	
	function generate()
	{
		$json = file_get_contents('php://input');
		if($json)
		{
			$json_dec 	= json_decode($json);	
			$longUrl 	= $json_dec->long_url;
		
		
			$permitted_chars= '0123456789abcdefghijklmnopqrstuvwxyz';
			$result 		= substr(str_shuffle($permitted_chars), 0, 6);
			$created_at 	= getCurrentDateAndTime();
			
			$base_url 	= "https://shorturl.com/";
			$result 	= $base_url.$result;
			
			$dataCheck 	= array("shorturl"=>$result);
			$isExists	=$this->msurl->isDataExists($dataCheck);
			
			if($isExists==0)
			{
				$data = array(
					"shorturl"=> $result,
					"longurl"=>$longUrl,
					"created_at"=>$created_at
				);
				$this->msurl->addData($data);
				$dataResponse = array(
							"short_url"=>$result,
							"created_at"=>$created_at,
							
				);
				$arrResponse = array(
					"data" => $dataResponse, 
					"status" => "success",
					"message" => ""
				);
			}
			else{
				$arrResponse = array(
					"data" => NULL, 
					"status" => "failed",
					"message" => "please try again. Short URL already exists"
				);
			}
			echo printJsonFormat(json_encode($arrResponse));
		}
	}
	
	function redirectUrl()
	{
		$json = file_get_contents('php://input'); //get param request
		if($json)
		{
			$json_dec 	= json_decode($json);	
			$shortUrl 	= $json_dec->short_url;
			
			//check short url is Exists in DB
			$dataCheck 	= array("searchby"=>"shorturl", "keyword"=>$shortUrl);
			$rs 		= $this->msurl->getOneDataByField($dataCheck); 
			
			if($rs)
			{	
				//get urlId and long url
				$urlId = 0;
				$longUrl="";
				foreach ($rs as $row){
					$longUrl 	= $row["longurl"];
					$urlId		= $row["urlid"];
				}
				
				//save history redirect
				$newdata = array(
					"urlId"=> $urlId,
					"access_at"=>getCurrentDateAndTime()
				);
				$this->accesshistory->addData($newdata);
				
				//redirect to the original link
				redirect($longUrl);
			}
			else
			{
				$arrResponse = array(
					"data" => NULL, 
					"status" => "failed",
					"message" => "please try again. Short URL not found"
				);
				echo printJsonFormat(json_encode($arrResponse));
			}
		}
	}
	
	function getStatisticRedirect()
	{
		$json = file_get_contents('php://input');
		if($json)
		{
			$json_dec 	= json_decode($json);	
			$shortUrl = $json_dec->short_url;
		
			$dataCheck 	= array("searchby"=>"shorturl", "keyword"=>$shortUrl);
			$rs 		= $this->msurl->getOneDataByField($dataCheck); 
			
			if($rs)
			{	
				$countStat	= 0;
				
				$urlId 		= 0;
				//get urlId short URL 
				
				foreach ($rs as $row){
					$urlId		= $row["urlid"];
				}
				
				//get statistic by url ID
				$searchdata = array(
					"urlid"=> $urlId
				);
				$countStat = $this->accesshistory->getStatistic($searchdata);
				
				$dataResponse = array(
					"redirect_count"=> $countStat,
					"created_at"=> getCurrentDateAndTime(),		
				);
								
				$arrResponse = array(
					"data" => $dataResponse, 
					"status" => "success"
				);
				echo printJsonFormat(json_encode($arrResponse));
			}
			else{
				$arrResponse = array(
					"data" => NULL, 
					"status" => "failed",
					"message" => "please try again. Short URL not found"
				);
				echo printJsonFormat(json_encode($arrResponse));
			}
			
		}
	}
}
?>