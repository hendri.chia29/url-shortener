# API 01
# Given a long URL, the service should generate a shorter and unique alias of it with
# exactly 6 alphanumeric characters and return it

URL : http://localhost/urlshortener/v1/core/generate
REQUEST :
{
    "long_url": "https://linkwebsite.com"
}


RESPONSE IF SUCCESS:
{
    "data": {
        "short_url": "https://shorturl.com/r7y4nk",
        "created_at": "2022-06-24 13:59:56"
    },
    "status": "success",
    "message": ""
}



# API 02
# Given a short link, the service should be able to redirect the request to the original link.

URL : http://localhost/urlshortener/v1/core/redirectUrl
REQUEST :
{
    "short_url": "https://shorturl.com/r7y4nk"
}


RESPONSE IF SUCCESS: redirect to orginal URL (long URL)

RESPONSE IF FAILED:
{
  "data": null,
  "status": "failed",
  "message": "please try again. Short URL not found"
}


# API 03
# Given a short link, the service should be able to return these stats: Redirect count & Created at

URL : http://localhost/urlshortener/v1/core/getStatisticRedirect

REQUEST :
{
    "short_url": "https://shorturl.com/r7y4nk"
}

RESPONSE IF SUCCESS:
{
    "data": {
        "redirect_count": "1",
        "created_at": "2022-06-24 13:59:56"
    },
    "status": "success"
}


RESPONSE IF FAILED:
{
    "data": null,
    "status": "failed",
    "message": "please try again. Short URL not found"
}



## Database design:


CREATE TABLE IF NOT EXISTS `msurl` (
  `urlid` int(11) NOT NULL auto_increment,
  `shorturl` varchar(35) NOT NULL,
  `longurl` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`urlid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `url_access_history` (
  `urlid` int(11) NOT NULL,
  `access_at` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `historyId` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`historyId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


## Tech Stack
PROGRAMMING LANGUAGE : PHP
FRAMEWORK : CODEIGNITER
DATABASE : mysql

PATH DIRECTORY CONTROLLER : application\controllers

PATH DIRECTORY MODEL : application\models
